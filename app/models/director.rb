class Director < ApplicationRecord
  has_many :movie_directors
  has_many :movies, through: :movie_directors

  def self.movie_counts
    all.map{|director| [director.name, director.movies.count] }
  end

  def to_s
    name
  end
end
